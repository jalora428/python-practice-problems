# Complete the can_skydive function so that determines if
# someone can go skydiving based on these criteria
#
# * The person must be greater than or equal to 18 years old, or
# * The person must have a signed consent form

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.


def can_skydive(age, consentform):
    if age >= 18:
        return True
    elif consentform:
        return True
    else:
        return False

age_person = 16
consent_form = False

print(can_skydive(age_person, consent_form))


