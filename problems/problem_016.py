# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.


#wrong way
def is_inside_bounds(x, y):
    if x > 0 and x < 10:
        return True
    if y > 0 and y < 10:
        return True
    else:
        return False

#print(is_inside_bounds(20, 30))


#correct way
def is_inside_bounds(x, y):
    if 0 < x < 10 and 0 < y < 10:
        return True
    else:
        return False
print(is_inside_bounds(9, 10))